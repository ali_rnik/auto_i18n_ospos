#!/bin/bash

# this is provided source translation from CUR_LANG.
CUR_LANG=en-US

# destination language for translation.
WILL_LANG=fa-IR

mkdir $WILL_LANG
for file in $(ls ./$CUR_LANG)
do
    cp ./$CUR_LANG/$file ./$WILL_LANG/$file
    if [[ $file == *.php ]]
    then
	while IFS= read -r line
	do
	    sent=$(echo "$line" | egrep -o "\ \".*\"\;$");
	    sent="${sent//;}";
	    transl=$(trans -no-bidi -no-auto -target fa -b "$sent");
	    if [ "$transl" == $WILL_LANG ]
	    then
		transl=""
	    fi
	    echo "$sent" "---->" "$transl";
	    echo "";
	    # speedy requests to google-translate will be blocked
	    sleep 5;
	    sed 's@'"$sent"'@'"$transl"'@' -i ./$WILL_LANG/$file
	done < ./$CUR_LANG/$file
    fi
done

