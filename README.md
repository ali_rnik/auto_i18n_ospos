What is this:

 This is a simple script which translate opensourcepos
(https://github.com/opensourcepos/opensourcepos) from english provided localisation
to wanted language (default case is Farsi). you can change request speed by modifying
the sleep time in the source code (it has defined to not get blocked by google)

 There are en-US and fa directories in this project which are only shows a sample
and are not part of the needed things to run the script (but you should provide a
provided localisation).
